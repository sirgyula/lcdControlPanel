from collections import OrderedDict	
from pprint import pprint

class menu:
	i1 = ('system status ', ['a','s','d','f'])
	i2 = ('kodi          ', ['-             ', 
							 'start         ',
							 'stop          ',
							 'refresh lib   ',
							 'clean lib     ',
							 'back          '])
	i3 = ('transmission  ', ['-             ', 
							 'start         ',
							 'stop          ',
							 'back          '])
	i4 = ('restart RPi   ', ['tf:restartPI'])
	i5 = ('shutdown RPi  ', ['tf:shutdownPI'])
	i6 = ('exit menu     ', ['tf:exit'])	
	
	menuTree = OrderedDict([i1, i2, i3, i4, i5, i6])

	@staticmethod
	def getMenuItem(menuLevel, itemIndex):
		if menuLevel == -1:
			return menu.getMainMenuItem(itemIndex)
		else:
			return menu.getSubMenuItem(menuLevel, itemIndex)
	
	@staticmethod
	def getMenuLength(menuLevel):
		if menuLevel == -1:
			return menu.getMainMenuLength()
		else:
			return menu.getSubMenuLength(menuLevel)
	
	@staticmethod
	def getSubMenuItem(subMenuIndex, itemIndex):
		return menu.menuTree[menu.getMainMenuItem(subMenuIndex)][itemIndex]
	
	@staticmethod
	def getSubMenuLength(subMenuIndex):
		return len(menu.menuTree[menu.getMainMenuItem(subMenuIndex)])
	
	@staticmethod
	def getMainMenuItem(index):
		return menu.menuTree.keys()[index]
		
	@staticmethod
	def getMainMenuLength():
		return len(menu.menuTree)
		
	@staticmethod
	def test():
		pprint(menu.menuTree)
		print('')
		pprint(menu.menuTree['kodi          '])
		print('')
		pprint(menu.menuTree['kodi          '][0])
		print('')
		pprint(menu.menuTree.keys())
		print('')
		pprint(menu.menuTree.keys()[0])
		print('')
		
		for item in menu.menuTree.keys():
			print(item)