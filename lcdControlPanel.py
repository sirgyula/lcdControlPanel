#!/usr/bin/python

#import
import RPi.GPIO as GPIO
import time
import sys
import subprocess
import os
#import Adafruit_CharLCD as Adafruit_CharLCD
import lcddriver

DOWN = 22
OK   = 27
UP   = 17

def startKodi():
  global line;
  
  os.system("sudo -u pi nohup kodi &");
  time.sleep(1)
  
  isKodiRunning = isProgramRunning("kodi");
  
  if(isKodiRunning == 1):
    line[0] = "kodi is on    ";
  else:
    line[0] = "kodi is off   ";
    
  msg(line[selectedLine - 1], line[selectedLine], 1);
  
def isProgramRunning(name):
  output = subprocess.getoutput('ps -A | awk \'/' + name + '/{print "1";exit}\'');

  if(output == "1"):
    return 1;
  else:
    return 0;

def mainMenu():
  global line;
  global selectedLine;
  global subMenuID;
  
  subMenuID = -1;
  
  line = {
  0: "system status ", 
  1: "kodi          ",
  2: "transmission  ",
  3: "restart       ",
  4: "shutdown      ",
  5: "exit menu     "};
  
  msg(line[selectedLine], line[selectedLine + 1], 0);

def kodiMenu():
  global line;
  global selectedLine;
  
  selectedLine = 0;
  
  line = {
  0: "              ", 
  1: "start         ",
  2: "stop          ",
  3: "refresh lib   ",
  4: "clean lib     ",
  5: "back          "};
  
  isKodiRunning = isProgramRunning("kodi");
  
  if(isKodiRunning == 1):
    line[0] = "kodi is on    ";
  else:
    line[0] = "kodi is off   ";
  
  msg(line[selectedLine], line[selectedLine + 1], 0);
  
def button(channel):
  global selectedLine;
  global xLocation;
  global line;
  
  if channel == UP:
    if selectedLine > 0:
      if xLocation == 1:                                         # also sorban van az X
        xLocation = 0;
        setX(xLocation);
        selectedLine -=1;
        msg(line[selectedLine], line[selectedLine + 1], 0)
      else:                                                      # felso sorban van az X
        selectedLine -= 1;
        msg(line[selectedLine], line[selectedLine + 1], 0)
        
  elif channel == OK:
    executeCommand(selectedLine);
    
  elif channel == DOWN:
    if selectedLine < len(line)-1:
      if xLocation == 0:                                         # felso sorban van az X
        xLocation = 1;
        selectedLine += 1;
        setX(xLocation);
      else:                                                      # also sorban van az X
        selectedLine += 1;
        msg(line[selectedLine - 1], line[selectedLine], 1);
  
def executeCommand(selectedLineNum):
  global line;
  global subMenuID;
  
  if subMenuID == -1:           # main menu
    if selectedLineNum == 0:
      print(line[selectedLineNum]);
      subMenuID = selectedLineNum;
    elif selectedLineNum == 1:
      kodiMenu();
      subMenuID = selectedLineNum;
    elif selectedLineNum == 2:
      print(line[selectedLineNum]);
      subMenuID = selectedLineNum;
    elif selectedLineNum == 3:
      print(line[selectedLineNum]);
      subMenuID = selectedLineNum;
    elif selectedLineNum == 4:
      print(line[selectedLineNum]);
      subMenuID = selectedLineNum;
    elif selectedLineNum == 5:
      print(line[selectedLineNum]);
      subMenuID = selectedLineNum;
    else:
      print('WAT?');
  elif subMenuID == 0:          # system status
    print('test');
  elif subMenuID == 1:          # kodi
    if selectedLineNum == 1:
      startKodi();                        # start
    elif selectedLineNum == 2:
      print(line[selectedLineNum]);       # stop
    elif selectedLineNum == 3:
      print(line[selectedLineNum]);       # refresh
    elif selectedLineNum == 4:
      print(line[selectedLineNum]);       # clean
    elif selectedLineNum == 5:
      print(line[selectedLineNum]);       # back
  elif subMenuID == 2:          # transmission
    print('test');
  elif subMenuID == 3:          # restart
    print('test');
  elif subMenuID == 4:          # shutdown
    print('test');
  elif subMenuID == 5:          # exit menu
    print('test');
  else:
    print('WAAAT??');

def msg(msgLine1, msgLine2, Xline):
  global lcd;
  global selectedLine;
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.message(msgLine1);
  
  lcd.setCursor(0, 1);
  lcd.message(msgLine2);
  
  setX(Xline);
  
def setX(currentLine):
  global lcd;
  
  if currentLine == 0:
    lcd.setCursor(15, 0);
    lcd.write4bits(0xFF, True);
    lcd.setCursor(15, 1);
    lcd.message(' ');
  else:
    lcd.setCursor(15, 0);
    lcd.message(' ');
    lcd.setCursor(15, 1);
    lcd.write4bits(0xFF, True);
  
def exit():
  global lcd;
  
  lcd.lcd_clear();
  lcd.message('Goodbye!')
  #GPIO.cleanup();
  
def main():
  global selectedLine
  selectedLine = 0

  global xLocation;
  xLocation = 0;
    
  #GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers

  #GPIO.setup(18, GPIO.OUT);
  #GPIO.output(18, True)
  
  #GPIO.setup(DOWN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  #GPIO.setup(OK, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  #GPIO.setup(UP, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  
  #GPIO.add_event_detect(DOWN, GPIO.FALLING, callback=button, bouncetime=200) # Wait for the input to go low, run the function when it does
  #GPIO.add_event_detect(OK, GPIO.FALLING, callback=button, bouncetime=200)
  #GPIO.add_event_detect(UP, GPIO.FALLING, callback=button, bouncetime=200)
  
  global lcd
  #lcd = Adafruit_CharLCD.Adafruit_CharLCD();
  lcd = lcddriver.lcd()
  lcd.lcd_clear()
  lcd.blink();
  lcd.begin(16,2);
  
  mainMenu();
  
  while True:
    time.sleep(1)

if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    print('')
    print('Goodbye!')
  finally:
    global lcd
    #lcd.noDisplay();
    time.sleep(1);
    exit();
