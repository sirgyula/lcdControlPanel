from time import *
from pprint import pprint

import gaugette.rotary_encoder
import gaugette.switch

import lcddriver
import commandRunner
import xmlMenu
import threadedWriter

import threading

class lcdControlPanel:
	def printMessage(self, msgLine1, msgLine2, Xline, Xcolumn = 15):
		self.lcd.clear()
		self.lcd.setCursor(0, 0)
		self.lcd.displayString(msgLine1)

		self.lcd.setCursor(0, 1)
		self.lcd.displayString(msgLine2)

		self.setX(Xline, Xcolumn)
		
	def setX(self, Xline, Xcolumn = 15):
		if self.inStatusMenu == False:
			if Xline == 0:
				self.lcd.setCursor(Xcolumn, 0)
				self.lcd.lcd_write(0x3C, 1)
				self.lcd.setCursor(Xcolumn, 1)
				self.lcd.displayString(' ')
			else:
				self.lcd.setCursor(Xcolumn, 0)
				self.lcd.displayString(' ')
				self.lcd.setCursor(Xcolumn, 1)
				self.lcd.lcd_write(0x3C, 1)
				
			self.xLocation = Xline
	
	def setXtf(self, Xcolumn):
		if Xcolumn == 6:
			self.lcd.setCursor(Xcolumn, 1)
			self.lcd.lcd_write(0x3C, 1)
			self.lcd.setCursor(13, 1)
			self.lcd.displayString(' ')
			self.leftTfOptionSelected = True
		elif Xcolumn == 13:
			self.lcd.setCursor(6, 1)
			self.lcd.displayString(' ')
			self.lcd.setCursor(Xcolumn, 1)
			self.lcd.lcd_write(0x3C, 1)
			self.leftTfOptionSelected = False
	
	def stepMenu(self, direction):
		if self.inTrueFalseMenu:
			if direction == -1:
				self.setXtf(6)
			elif direction == 1:
				self.setXtf(13)
			else:
				print('wat?')
		elif self.inStatusMenu:
			if direction == -1:
				if self.selectedLine > 0:
					self.selectedLine -= 2
			elif direction == 1:
				if self.selectedLine < self.menu.getMenuLength(self.currentMenuLevel) - 2:
					self.selectedLine += 2
					self.dynamicThread.setNewDynamicData(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), self.menu.getSubMenuItemCommand(self.currentMenuLevel, self.selectedLine + 1))
			else:
				print('wat?')
		else:
			if direction == -1:
				if self.selectedLine > 0:
					if self.xLocation == 1:                                         # also sorban van az X
						self.xLocation = 0
						self.setX(self.xLocation)
						self.selectedLine -=1
					else:                                                      		# felso sorban van az X
						self.selectedLine -= 1
						self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), 0)
			elif direction == 1:
				if self.selectedLine < self.menu.getMenuLength(self.currentMenuLevel) - 1:
					if self.xLocation == 0:                                         # felso sorban van az X
						self.xLocation = 1
						self.selectedLine += 1
						self.setX(self.xLocation)
					else:                                                      		# also sorban van az X
						self.selectedLine += 1 
						self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine - 1), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), 1)
			else:
				print('wat?')
		
	def command(self):
		cmd = self.menu.getMenuCommand(self.currentMenuLevel, self.selectedLine)
		print('cmd: {0}'.format(cmd))
		commandRunner.commandRunner.executeCommand(cmd)
		
	def buttonPress(self):
		if self.inTrueFalseMenu:
			if self.leftTfOptionSelected:
				self.command()
			else:
				print('nope')
				
			self.inTrueFalseMenu = False
			self.goBack()
		elif self.inStatusMenu:
			self.dynamicThread.stop()
			self.dynamicThread.join()
			
			self.goBack()
			self.inStatusMenu = False
			self.setX(0)
		else:
			type = self.menu.getMenuType(self.currentMenuLevel, self.selectedLine)
			print('type: {0}'.format(type))

			if type == 'submenu':															# entering submenu
				print('entering menu {0}'.format(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine)))
				self.currentMenuLevel = self.selectedLine
				self.selectedLine = 0
				self.xLocation = 0
				self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), 0) 
			elif type == 'tf':																# true/false selected
				self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), '  YES     NO    ', 1, 13)
				self.inTrueFalseMenu = True
			elif type == 'static':
				#handled nowhere, no need
				pass
			elif type == 'dynamic':
				#handled in the xmlMenu.py
				pass
			elif type == 'status':
				self.inStatusMenu = True
				print('entering menu {0}'.format(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine)))
				self.currentMenuLevel = self.selectedLine
				self.selectedLine = 0
				self.xLocation = 0
				self.dynamicThread = threadedWriter.threadedWriter(self.lcd, 0.2, self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), self.menu.getSubMenuItemCommand(self.currentMenuLevel, self.selectedLine + 1))
				self.dynamicThread.start()
			elif type == 'cmd':
				self.command()
			elif type == 'back':
				self.inStatusMenu = False
				self.goBack()
			else:
				print('unknown menuitem type')

	def goBack(self):
		print('back selected')
		self.selectedLine = 0
		self.currentMenuLevel = -1
		self.xLocation = 0
		self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), 0) 

	def start(self):
		encoder = gaugette.rotary_encoder.RotaryEncoder.Worker(self.A_PIN, self.B_PIN)
		encoder.start()
		switch = gaugette.switch.Switch(self.SW_PIN)
		last_state = None
		fire = False															# used to avoid the double trigger of the rotaryencoder
		
		while True:
			delta = encoder.get_delta()
			if delta!=0:
				if fire == True:
					self.restartTimer()
					self.stepMenu(delta)
					fire = False
				else:
					fire = True
		
			sw_state = switch.get_state()
			if sw_state != last_state:
				if sw_state == 1:
					self.restartTimer()
					self.buttonPress()
				sleep(0.2)
			last_state = sw_state
			
	def testLcd(self):
		self.lcd.lcd_display_string("Tutorials-      ", 1)
		self.lcd.lcd_display_string("raspberrypi.de  ", 2)

		sleep(1)
		self.lcd.clear()

		self.lcd.setCursor(1, 0)
		self.lcd.displayString("as")

		sleep(1)
		self.lcd.clear()
		
	def testCustomChars(self):
		self.lcd.clear()
		sleep(0.5)
		self.lcd.setCursor(0, 0)
			
		for num in range(0,3):
			self.lcd.lcd_write(0x00, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
			self.lcd.lcd_write(0x01, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
			self.lcd.lcd_write(0x02, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
			self.lcd.lcd_write(0x03, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
			self.lcd.lcd_write(0x02, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
			self.lcd.lcd_write(0x01, 1);
			self.lcd.setCursor(0, 0)
			sleep(0.2)
		
	def startTimer(self):
		self.t = threading.Timer(10.0, self.lcd.noBacklight)	
		self.t.start()
	
	def restartTimer(self):
		self.t.cancel()
		sleep(0.2)
		self.startTimer()
		
	def __init__(self):
		self.A_PIN  = 29
		self.B_PIN  = 28
		self.SW_PIN = 27
		
		self.selectedLine = 0
		self.xLocation = 0
		self.currentMenuLevel = -1
		self.inTrueFalseMenu = False
		self.leftTfOptionSelected = False
		self.inStatusMenu = False
		
		customChars = [
			[0x01, 0x02, 0x04, 0x02, 0x01, 0x00, 0x00, 0x00],
			[0x00, 0x01, 0x02, 0x04, 0x02, 0x01, 0x00, 0x00],
			[0x00, 0x00, 0x01, 0x02, 0x04, 0x02, 0x01, 0x00],
			[0x00, 0x00, 0x00, 0x01, 0x02, 0x04, 0x02, 0x01]
			]
			
		self.lcd = lcddriver.lcd()
		self.lcd.begin(16,2)
		self.lcd.clear()
		self.lcd.addCustomChars(customChars)
	
		self.menu = xmlMenu.xmlMenu()
		
		self.printMessage(self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine), self.menu.getMenuItem(self.currentMenuLevel, self.selectedLine + 1), 0) 
		
		self.startTimer()
		
		print('initialized')