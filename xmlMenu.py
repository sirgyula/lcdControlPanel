import xml.etree.ElementTree
import commandRunner

class xmlMenu:
	def getMenuItem(self, menuLevel, itemIndex):
		if menuLevel == -1:
			return self.getMainMenuItem(itemIndex)
		else:
			return self.getSubMenuItem(menuLevel, itemIndex)

	def getMenuLength(self, menuLevel):
		if menuLevel == -1:
			return self.getMainMenuLength()
		else:
			return self.getSubMenuLength(menuLevel)

	def getMenuType(self, menuLevel, itemIndex):
		if menuLevel == -1:
			return self.getMainMenuItemType(itemIndex)
		else:
			return self.getSubMenuItemType(menuLevel, itemIndex)

	def getMenuCommand(self, menuLevel, itemIndex):
		if menuLevel == -1:
			return self.getMainMenuItemCommand(itemIndex)
		else:
			return self.getSubMenuItemCommand(menuLevel, itemIndex)


			
			

			
	def getMainMenuItem(self, index):
		return self.root[index].get('text')

	def getMainMenuLength(self):
		return len(self.root.findall('MenuItem'))

	def getMainMenuItemType(self, index):
		return self.root[index].get('type')
		
	def getMainMenuItemCommand(self, index):
		return self.root[index].get('command')
		
		
		
		
		
	def getSubMenuItem(self, subMenuIndex, itemIndex):
		if self.getSubMenuItemType(subMenuIndex, itemIndex) == 'check':
			return commandRunner.commandRunner.getDynamicString(self.getSubMenuItemCommand(subMenuIndex, itemIndex), self.root[subMenuIndex][itemIndex].get('text'))
		else:
			return self.root[subMenuIndex][itemIndex].get('text')
		
	def getSubMenuLength(self, subMenuIndex):
		return len(self.root[subMenuIndex].findall('MenuItem'))

	def getSubMenuItemType(self, subMenuIndex, itemIndex):
		return self.root[subMenuIndex][itemIndex].get('type')
		
	def getSubMenuItemCommand(self, subMenuIndex, itemIndex):
		return self.root[subMenuIndex][itemIndex].get('command')
		
		
		
		
		
	def __init__(self):
		self.tree = xml.etree.ElementTree.parse('menu.xml')
		self.root = self.tree.getroot()
		