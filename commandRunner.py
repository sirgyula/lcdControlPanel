import time
import sys
import subprocess
import os
import smbus

class commandRunner:
	commands = dict(restartPI='touch f_one',
					shutdownPI='touch f_two',
                    exit='touch f_three',
					startkodi='touch f_four',
					stopkodi='touch f_five',
					refreshkodi='touch f_six',
					cleankodi='touch f_seven',
					starttransmission='touch f_eight',
					stoptransmission='touch f_nine',
					checkkodi='kodi',
					checktransmission='transmission',
					gettemperature='',
					getnetwork='',
					getstorage=''
					)

	i2c = smbus.SMBus(1)
					
	@staticmethod
	def getDynamicString(cmd, pattern):
		if cmd.startswith('check'):
			appName = commandRunner.commands.get(cmd)

			asdf = 'ps -A | awk \'/' + appName + '/{print "1";exit}\'; exit 0'
			print(asdf)
			output = subprocess.check_output(asdf, stderr=subprocess.STDOUT, shell=True)
			
			if(output == '1\n'):
				return 'RUNNING       '
			else:
				return 'STOPPED       '
		elif cmd == 'gettemperature':
			return pattern.format(commandRunner.cpu_temp(), commandRunner.sot23_temp(), commandRunner.to92_temp())
		elif cmd == 'getnetwork':
			output = subprocess.check_output('sudo transmission-remote -n sirgyula:raspberry -l | grep "Sum: "; exit 0', stderr=subprocess.STDOUT, shell=True)
			array = filter(None, output.split(' '))
			upspeed = array[3].split('.')[0]
			downspeed = array[4].split('.')[0]
		
			return pattern.format(downspeed.rjust(4), upspeed.rjust(4))
		elif cmd == 'getstorage':
			output = subprocess.check_output('df -h /dev/sda1 | grep sda1; exit 0', stderr=subprocess.STDOUT, shell=True)
			hdd = filter(None, output.split(' '))[3]
			
			output = subprocess.check_output('df -h / | grep root; exit 0', stderr=subprocess.STDOUT, shell=True)
			sd = filter(None, output.split(' '))[3]

			return pattern.format(sd, hdd)
		else:
			print('unknown command to run')
					
	@staticmethod
	def executeCommand(cmd):
		commandToRun = commandRunner.commands.get(cmd)
		print(commandToRun)
		commandRunner.execute(commandToRun)
		
	@staticmethod
	def execute(cmd):
		os.system('sudo -u pi {0}'.format(cmd))
		time.sleep(1)
	
	@staticmethod
	def sot23_temp():
		time.sleep(0.1)
		data = commandRunner.i2c.read_byte_data(0x69, 0x0C)
		data = format(data,"02x")
		return data
	
	@staticmethod
	def to92_temp():
		time.sleep(0.1)
		data = commandRunner.i2c.read_byte_data(0x69, 0x0d)
		data = format(data,"02x")
		return data
		
	@staticmethod
	def cpu_temp():
		time.sleep(0.1)
		output = subprocess.check_output('vcgencmd measure_temp; exit 0', stderr=subprocess.STDOUT, shell=True)
		output = output.replace('temp=', '')
		return output[:2]
