import time
import threading
import commandRunner

class threadedWriter(threading.Thread):
	def __init__(self, lcdParam, delay, staticText, pattern, command):
		threading.Thread.__init__(self)
		
		self.lcd = lcdParam
		self.delay = delay
		self.staticText = staticText
		self.pattern = pattern
		self.command = command
		
		self.isDataChanged = True
		
		self.exitFlag = 0
	
	def run(self):
		self.exitFlag = 0
		self.lcd.clear()
			
		while self.exitFlag == 0:
			dynamicData = commandRunner.commandRunner.getDynamicString(self.command, self.pattern)
			self.printMessage(self.staticText, dynamicData)
			
			for i in range(1, 10):
				time.sleep(self.delay/10)
				if self.exitFlag:
					break
						
		print('Stopped')
		
	def stop(self):
		self.exitFlag = 1
		
	def printMessage(self, msg1, msg2):
		self.lcd.setCursor(0, 0)
		self.lcd.displayString(msg1)
		self.lcd.setCursor(0, 1)
		self.lcd.displayString(msg2)
	
	def setNewDynamicData(self, staticText, pattern, command):
		self.staticText = staticText
		self.isDataChanged = True
		self.pattern = pattern
		self.command = command
