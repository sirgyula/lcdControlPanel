﻿import i2c_lib
from time import *

# LCD Address
ADDRESS = 0x27

# commands
LCD_CLEARDISPLAY = 0x01
LCD_RETURNHOME = 0x02
LCD_ENTRYMODESET = 0x04
LCD_DISPLAYCONTROL = 0x08
LCD_CURSORSHIFT = 0x10
LCD_FUNCTIONSET = 0x20
LCD_SETCGRAMADDR = 0x40
LCD_SETDDRAMADDR = 0x80

# flags for display entry mode
LCD_ENTRYRIGHT = 0x00
LCD_ENTRYLEFT = 0x02
LCD_ENTRYSHIFTINCREMENT = 0x01
LCD_ENTRYSHIFTDECREMENT = 0x00

# flags for display on/off control
LCD_DISPLAYON = 0x04
LCD_DISPLAYOFF = 0x00
LCD_CURSORON = 0x02
LCD_CURSOROFF = 0x00
LCD_BLINKON = 0x01
LCD_BLINKOFF = 0x00

# flags for display/cursor shift
LCD_DISPLAYMOVE = 0x08
LCD_CURSORMOVE = 0x00
LCD_MOVERIGHT = 0x04
LCD_MOVELEFT = 0x00

# flags for function set
LCD_8BITMODE = 0x10
LCD_4BITMODE = 0x00
LCD_2LINE = 0x08
LCD_1LINE = 0x00
LCD_5x10DOTS = 0x04
LCD_5x8DOTS = 0x00

# flags for backlight control
LCD_BACKLIGHT = 0x08
LCD_NOBACKLIGHT = 0x00

En = 0b00000100 # Enable bit
Rw = 0b00000010 # Read/Write bit
Rs = 0b00000001 # Register select bit

class lcd:
	#initializes objects and lcd
	def __init__(self):
		self.customCharsAdded = False
	
		self.lcd_device = i2c_lib.i2c_device(ADDRESS)

		#self.lcd_write(0x03)
		#self.lcd_write(0x03)
		#self.lcd_write(0x03)
		#self.lcd_write(0x02)
		
		#self.lcd_write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5x8DOTS | LCD_4BITMODE)
		#self.lcd_write(LCD_DISPLAYCONTROL | LCD_DISPLAYON)
		#self.lcd_write(LCD_CLEARDISPLAY)
		#self.lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT)

		self.lcd_write(0x33)  # initialization
		self.lcd_write(0x32)  # initialization
		self.lcd_write(0x28)  # 2 line 5x7 matrix
		self.lcd_write(0x0C)  # turn cursor off 0x0E to enable cursor
		self.lcd_write(0x06)  # shift cursor right

		self.displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF

		self.displayfunction = LCD_4BITMODE | LCD_2LINE | LCD_5x8DOTS

		# Initialize to default text direction (for romance languages)
		self.displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT
		self.lcd_write(LCD_ENTRYMODESET | self.displaymode)  # set the entry mode

		sleep(0.2)

	# clocks EN to latch command
	def lcd_strobe(self, data):
		self.lcd_device.write_cmd(data | En | LCD_BACKLIGHT)
		sleep(.0005)
		self.lcd_device.write_cmd(((data & ~En) | LCD_BACKLIGHT))
		sleep(.0001)

	def lcd_write_four_bits(self, data):
		self.lcd_device.write_cmd(data | LCD_BACKLIGHT)
		self.lcd_strobe(data)

	def noBacklight(self):
		self.lcd_device.write_cmd(LCD_NOBACKLIGHT)
		
	def backlight(self):
		self.lcd_device.write_cmd(LCD_BACKLIGHT)
		
	# write a command to lcd
	def lcd_write(self, cmd, mode=0):
		self.lcd_write_four_bits(mode | (cmd & 0xF0))
		self.lcd_write_four_bits(mode | ((cmd << 4) & 0xF0))

	def displayString(self, string):
		for char in string:
			self.lcd_write(ord(char), Rs)

	# put string function
	def lcd_display_string(self, string, line):
		if line == 1:
			self.lcd_write(0x80)
		if line == 2:
			self.lcd_write(0xC0)
		if line == 3:
			self.lcd_write(0x94)
		if line == 4:
			self.lcd_write(0xD4)

		for char in string:
			self.lcd_write(ord(char), Rs)

	# clear lcd and set to home
	def clear(self):
		self.lcd_write(LCD_CLEARDISPLAY)
		self.lcd_write(LCD_RETURNHOME)

	def begin(self, cols, lines):
		if (lines > 1):
			self.numlines = lines
			#self.displayfunction |= self.LCD_2LINE

	def setCursor(self, col, row):
		self.row_offsets = [0x00, 0x40, 0x14, 0x54]
		if row > self.numlines:
			row = self.numlines - 1  # we count rows starting w/0
		self.lcd_write(LCD_SETDDRAMADDR | (col + self.row_offsets[row]))

	def home(self):
		self.lcd_write(LCD_RETURNHOME)  # set cursor position to zero
		self.delayMicroseconds(3000)  # this command takes a long time!
		
	def delayMicroseconds(self, microseconds):
		seconds = microseconds / float(1000000)  # divide microseconds by 1 million for seconds
		sleep(seconds)
		
	def noDisplay(self):
		""" Turn the display off (quickly) """
		self.displaycontrol &= ~LCD_DISPLAYON
		self.lcd_write(LCD_DISPLAYCONTROL | self	.displaycontrol)
		
	def display(self):	
		""" Turn the display on (quickly) """
		self.displaycontrol |= LCD_DISPLAYON
		self.lcd_write(LCD_DISPLAYCONTROL | self.displaycontrol)
		
	def noCursor(self):
		""" Turns the underline cursor off """
		self.displaycontrol &= ~LCD_CURSORON
		self.lcd_write(LCD_DISPLAYCONTROL | self.displaycontrol)
		
	def cursor(self):	
		""" Turns the underline cursor on """
		self.displaycontrol |= LCD_CURSORON
		self.lcd_write(LCD_DISPLAYCONTROL | self.displaycontrol)
		
	def noBlink(self):	
		""" Turn the blinking cursor off """
		self.displaycontrol &= ~LCD_BLINKON
		self.lcd_write(LCD_DISPLAYCONTROL | self.displaycontrol)
		
	def blink(self):	
		""" Turn the blinking cursor on """
		self.displaycontrol |= LCD_BLINKON
		self.lcd_write(LCD_DISPLAYCONTROL | self.displaycontrol)
		
	def DisplayLeft(self):
		""" These commands scroll the display without changing the RAM """
		self.lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT)
		
	def scrollDisplayRight(self):
		""" These commands scroll the display without changing the RAM """
		self.lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT)
		
	def leftToRight(self):	
		""" This is for text that flows Left to Right """
		self.displaymode |= LCD_ENTRYLEFT
		self.lcd_write(LCD_ENTRYMODESET | self.displaymode)
		
	def rightToLeft(self):	
		""" This is for text that flows Right to Left """
		self.displaymode &= ~LCD_ENTRYLEFT
		self.lcd_write(LCD_ENTRYMODESET | self.displaymode)
		
	def autoscroll(self):	
		""" This will 'right justify' text from the cursor """
		self.displaymode |= LCD_ENTRYSHIFTINCREMENT
		self.lcd_write(LCD_ENTRYMODESET | self.displaymode)
		
	def noAutoscroll(self):	
		""" This will 'left justify' text from the cursor """
		self.displaymode &= ~LCD_ENTRYSHIFTINCREMENT
		self.lcd_write(LCD_ENTRYMODESET | self.displaymode)
		
	def addCustomChars(self, array):
		if self.customCharsAdded == False:
			self.customCharsAdded = True
			self.lcd_write(0x40);
			
			for arrayItem in array:
				for customChar in arrayItem:
					self.lcd_write(customChar, 1)
		else:
			print('Custom chars already added')
		